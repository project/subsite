<?php

/**
 * Created by PhpStorm.
 * User: andy
 * Date: 15/01/2016
 * Time: 23:12
 */

namespace Drupal\subsite;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;

interface SubsitePluginInterface extends ConfigurableInterface, PluginFormInterface {
  public function blockPrerender($build, $node, $subsite_node);
  public function nodeViewAlter(array &$build, EntityInterface $node, EntityViewDisplayInterface $display);
  public function pageAttachmentsAlter(array &$attachments);
}
